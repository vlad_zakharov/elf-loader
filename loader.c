#include <errno.h>
#include <fcntl.h>
#include <linux/elf.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <inttypes.h>
#include <sys/mman.h>
#include <unistd.h>

extern errno;

#define ELF_MIN_ALIGN  sysconf(_SC_PAGESIZE)

#define ELF_PAGESTART(_v) ((_v) & ~(unsigned long)(ELF_MIN_ALIGN-1))
#define ELF_PAGEOFFSET(_v) ((_v) & (ELF_MIN_ALIGN-1))
#define ELF_PAGEALIGN(_v) (((_v) + ELF_MIN_ALIGN - 1) & ~(ELF_MIN_ALIGN - 1))


/*
 * This example works with 64-bit elfs
 */

struct elf_im {
	void *base;
	int (*enter)(int , char**);
};

Elf64_Shdr *shdrs;
Elf64_Sym *syms;
int syms_count;
char* shstr_table;
char* str_table;

int (*foo)(void);

int verify_elf(unsigned char *e_ident) {
	/* Verify magic bytes and 64-bit elf */
	if (strncmp((const char*)e_ident, ELFMAG, 4) || e_ident[EI_CLASS] < ELFCLASS64) {
		return -ENOEXEC;
	}

	return 0;
}

void *read_section_table(int elf_fd, off_t offs, ssize_t size)
{
	int readc = -1;
	void *ptr;

	ptr = malloc(size);
	lseek(elf_fd, offs, SEEK_SET);
	readc = read(elf_fd, ptr, size);
	if (readc < 0) {
		perror("read");
		exit(EXIT_FAILURE);
	}

	return ptr;
}

void *load_elf(const char* path, struct elf_im *img)
{
	void *addr, *map_addr;
	int elf_fd, readc, ret, i, k = 0, prot , flags;
	unsigned long size = 0, offs;
	Elf64_Ehdr header;
	Elf64_Phdr *phdrs;

	elf_fd = open("hello_world", O_RDWR | O_SYNC);
	if (elf_fd < 0) {
		perror("open");
		return MAP_FAILED;
	}

	/* Read elf identification field to verify it */
	readc = read(elf_fd, header.e_ident, sizeof(header.e_ident));
	if (readc < 0) {
		perror("read");
		exit(EXIT_FAILURE);
	}

	ret = verify_elf(header.e_ident);
	if (ret) {
		errno = (-ret);
		perror("Wrong elf format");
	}

	/* Read whole elf heder */
	lseek(elf_fd, 0, SEEK_SET);
	readc = read(elf_fd, &header, sizeof(Elf64_Ehdr));
	if (readc < 0) {
		perror("read");
		exit(EXIT_FAILURE);
	}

	shdrs = read_section_table(elf_fd, header.e_shoff,
			header.e_shnum * header.e_shentsize);

	shstr_table = read_section_table(elf_fd,
			shdrs[header.e_shstrndx].sh_offset,
			shdrs[header.e_shstrndx].sh_size);

	for (i = 0; i < header.e_shnum; i++) {
		if (!strcmp(shstr_table + shdrs[i].sh_name, ".strtab")) {
			str_table = read_section_table(elf_fd, shdrs[i].sh_offset,
					shdrs[i].sh_size);
		}
		if (!strcmp(shstr_table + shdrs[i].sh_name, ".symtab")) {
			syms = read_section_table(elf_fd, shdrs[i].sh_offset,
					shdrs[i].sh_size);
			syms_count = shdrs[i].sh_size / shdrs[i].sh_entsize;
		}
	}


	phdrs = malloc(header.e_phnum * sizeof(Elf64_Phdr));
	lseek(elf_fd, header.e_phoff, SEEK_SET);
	readc = read(elf_fd, phdrs, header.e_phnum * sizeof(Elf64_Phdr));
	if (readc < 0) {
		perror("read");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < header.e_phnum; i++ ) {
		if (phdrs[i].p_type == PT_LOAD) {
			addr = (void *) ELF_PAGESTART((unsigned long) img->base + phdrs[i].p_vaddr);
			size = ELF_PAGEALIGN(phdrs[i].p_memsz + ELF_PAGEOFFSET(phdrs[i].p_vaddr));
			offs = phdrs[i].p_offset - ELF_PAGEOFFSET(phdrs[i].p_vaddr);

			prot = 0;
			if (phdrs[i].p_flags && PF_R)
				prot |= PROT_READ;
			if (phdrs[i].p_flags && PF_W)
				prot |= PROT_WRITE;
			if (phdrs[i].p_flags && PF_X)
				prot |= PROT_EXEC;

			flags = MAP_PRIVATE;
			if (img->base)
				flags |= MAP_FIXED;
			map_addr = mmap(addr, size, prot, flags, elf_fd, offs);
			if (map_addr == MAP_FAILED) {
				return MAP_FAILED;
			}

			if (k == 0) {
				img->base = (void *) map_addr - ELF_PAGESTART(phdrs[i].p_vaddr);
				img->enter = (void *) header.e_entry + (unsigned long) img->base;
			}
			k++;
		}
	}
	close(elf_fd);

	return img->base;
}

int main (int argc,  char** argv)
{

	struct elf_im image;
	void* ret;
	int i, x;

	char elf_path[1024], *func = malloc(80);
	strcpy(elf_path, "hello_world");
	strcpy(func, "func1");

	if (argc > 1)
		strcpy(func, argv[1]);
	if (argc > 2)
		strcpy(elf_path, argv[2]);



	ret = load_elf(elf_path, &image);
	if (ret == MAP_FAILED){
		perror("mmap");
	}

	if (argc > 1) {
		for (i = 0; i < syms_count; i++) {
			if (!strcmp(func, str_table + syms[i].st_name)) {
				foo = (void *) image.base + syms[i].st_value;
				x = foo();
				printf("%s from %s and it returned %d\n", func, elf_path, x);
			}
		}
	}

	return 0;
}
