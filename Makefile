CC=gcc
CFLAGS=-g3 -Wall

all: hello_world loader

hello_world: hello_world.c
	$(CC) $(CFLAGS) hello_world.c -o hello_world

loader: loader.c
	$(CC) $(CFLAGS) loader.c -o loader

clean:
	rm hello_world loader

