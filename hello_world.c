
int func1()
{
	return 1;
}

int func2()
{
	int a,b;
	a = 1;
	b = 2;
	a = a + b;
	b = a + b;
	return 22;
}

int func3()
{
	int x = func2();
	return 333 + x;
}

int main (int argc, char **argv)
{
	func1();
	func2();
	func3();
}
